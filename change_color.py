
import tkinter as tk
from tkinter import filedialog
import numpy as np
from PIL import ImageFont, Image

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    root = tk.Tk()
    root.withdraw()
    fpath = filedialog.askopenfilename()
    print("file: {}\n".format(fpath))

    font_size = 50
    font_loc = "C:\\Windows\\Fonts\\simhei.ttf"
    font = ImageFont.truetype(font_loc, font_size)

    # img = cv2.imdecode(np.fromfile(fpath, dtype=np.uint8), cv2.IMREAD_COLOR)
    img = Image.open(fpath)
    # img_pil = Image.fromarray(img)
    img_pil = img.convert("RGBA")

    width, height = img_pil.size[0], img_pil.size[1]

    img_array = np.array(img_pil)
    print(img_array[96][115])
    print(img_pil.getpixel((115, 96)))
    print(img_array.size)
    print(img_pil.size)

    for i in range(0, width):
        for j in range(0, height):
            c = img_pil.getpixel((i, j))
            # img_array[j][i] = [0, 255, 0, c[3]]
            if c[2] - c[1] > c[1] - c[0] and c[1] < 200:
                img_array[j][i] = [c[2], c[0], c[1], c[3]]
            else:
                img_array[j][i] = [c[0], c[1], c[2], c[3]]

    img_res = Image.fromarray(img_array)

    img_res.show()

    spath = filedialog.asksaveasfilename()
    print("file: {}\n".format(spath))
    img_res.save(spath)
