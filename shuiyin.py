# coding=utf-8
# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import tkinter as tk
from tkinter import filedialog
import cv2
from PIL import ImageFont, ImageDraw, Image
import numpy as np


def watermark(width, height, text, font, color="black"):
    w = int(width * 2)
    h = int(height * 2)
    bkg = Image.new("RGBA", (w, h), (0, 0, 0, 0))
    draw = ImageDraw.Draw(bkg)
    for x in range(0, w, 1000):
        for y in range(0, h, 500):
            draw.text((x, y), text, font=font, fill=(0, 0, 0, 40))
    # rotate = bkg.rotate(30)
    bkg_array = np.array(bkg)
    M = cv2.getRotationMatrix2D((w/2.0, h/2.0), -30, 1.0)
    rotate = cv2.warpAffine(bkg_array, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
    return Image.fromarray(rotate).crop((0.3*width, 0.3*height, 1.3*width, 1.3*height))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    root = tk.Tk()
    root.withdraw()
    fpath = filedialog.askopenfilename()
    print("file: {}\n".format(fpath))

    font_size = 50
    font_loc = "C:\\Windows\\Fonts\\simhei.ttf"
    font = ImageFont.truetype(font_loc, font_size)

    # img = cv2.imdecode(np.fromfile(fpath, dtype=np.uint8), cv2.IMREAD_COLOR)
    img = Image.open(fpath)
    # img_pil = Image.fromarray(img)
    img_pil = img.convert("RGBA")

    width, height = img_pil.size[0], img_pil.size[1]
    draw = ImageDraw.Draw(img_pil)

    mask = Image.new("L", img_pil.size, 128)
    mark = watermark(width, height, "仅用于公司地址迁移业务", font=font)
    img_pil = Image.alpha_composite(img_pil, mark)
    # img_pil.paste(mark, (0, 0), mask)

    img_pil.show()

    spath = filedialog.asksaveasfilename()
    print("file: {}\n".format(spath))
    img_pil.save(spath)



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
