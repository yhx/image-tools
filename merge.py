
import tkinter as tk
from tkinter import filedialog
import cv2
from PIL import ImageFont, ImageDraw, Image
import numpy as np

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    root = tk.Tk()
    root.withdraw()
    fpath1 = filedialog.askopenfilename()
    print("file: {}\n".format(fpath1))

    fpath2 = filedialog.askopenfilename()
    print("file: {}\n".format(fpath2))

    img = Image.open(fpath1)
    img1_rgba = img.convert("RGBA")
    img = Image.open(fpath2)
    img2_rgba = img.convert("RGBA")

    img2_rgba.resize(img1_rgba.size)

    img1_rgba.alpha_composite(img2_rgba)

    img1_rgba.show()

    spath = filedialog.asksaveasfilename()
    print("file: {}\n".format(spath))
    img1_rgba.save(spath)
